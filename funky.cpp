#include <iostream>
#include <eigen3/Eigen/Core>

/*
A technique used in ompl but not necessary ensured by the standard.
https://stackoverflow.com/questions/281045/do-class-struct-members-always-get-created-in-memory-in-the-order-they-were-decl
*/

class Funky
{
  public:
    Funky(double x = 1.0, double y = 2.0, double z = 3.0) :
        _x(x), _y(y), _z(z) {}
    
    double* getValueAtIndex(const unsigned int index)
    {
        return index < 3 ? &(_x) + index : nullptr;
    }

    unsigned int getDim() {return 3;}

  protected:
    double _x;
    double _y;
    double _z;    
};

        

int main()
{
    std::cout << "Let's try some funky stuff." << std::endl;

    auto bob = Funky();

    for (int i = 0; i < bob.getDim(); i++)
    {
        std::cout << *(bob.getValueAtIndex(i)) << ", ";
    }
    std::cout << std::endl;

    auto v = Eigen::Map<Eigen::VectorXd>(bob.getValueAtIndex(0), bob.getDim());
    std::cout << v << std::endl;
}